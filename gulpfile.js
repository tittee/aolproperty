'use strict';

var gulp 				= require('gulp');
var sass 				= require('gulp-sass');
var jade 				= require('gulp-jade');

var cssnano 		= require('gulp-cssnano');
var sourcemaps = require('gulp-sourcemaps');

var browserSync = require('browser-sync');
var imagemin 		= require('gulp-imagemin'); // Needed to Optimize Images

var cache 			= require('gulp-cache'); // Speeds up image optimze

var runSequence = require('run-sequence');
var fs = require('fs');

var useref = require('gulp-useref');
var gulpIf = require('gulp-if');
var uglify = require('gulp-uglify');
var minifyCss = require('gulp-clean-css');
/**
 * Sass task for live injecting into all browsers
 */
gulp.task('sass', function () {
	return gulp.src('./app/scss/**/*.scss')
		.pipe(sass())
		.pipe(gulp.dest('./template/assets/css/'))
		.pipe(browserSync.reload({
			stream: true
		}));
})

/**
 * Compile jade files into HTML
 */
gulp.task('jade', function () {
	return gulp.src('./app/jade/**/*.jade')
		.pipe(jade({
			pretty: true,
			//locals: JSON.parse( fs.readFileSync('./app/json/data.json', { encoding: 'utf8' }) ),
		}))
		.pipe(gulp.dest('./template/html/'))
})

/**
 * Watches file for changes
 */

gulp.task('watch', ['browserSync', 'sass', 'images', 'jade'], function () { // Include broswerSync with sass to get tasks to run together
	gulp.watch('./app/scss/**/*.scss', ['sass']);
	gulp.watch('./app/images/**/*', ['images']);
	// Reloads the browser whenever HTML or JS files change
	gulp.watch('./app/jade/**/*.jade', ['jade']);
	gulp.watch('./app/js/*.js', browserSync.reload);
	// Other watchers
});

// Live Reload
gulp.task('browserSync', function () {
	browserSync({
		startPath: '/',
		server: {
			// baseDir: './template/html/', // Directory for server
			// index: "index.html",
			baseDir: ["./", "./", "./", "./template/html/"]   //added multiple directories
		},
	})
	gulp.watch("./assets/css/*.css", ['css']);
	gulp.watch("./assets/js/*.js", ['script']);
	gulp.watch("./assets/images/*", ['images']);
	gulp.watch("./template/html/*.html").on("change", browserSync.reload); // For changes in html file to activate LiveReload
});

/***
 *  Image Optimizer
 ***/
gulp.task('images', function () {
	return gulp.src('./app/images/*.+(png|jpg|gif|svg)') // Path to images and included image types
		.pipe(imagemin())
		.pipe(gulp.dest('./template/assets/images/')) // Destination for optimized images
});

/***
 *  Speed Up Image Optimizer
 ***/
gulp.task('images', function () {
	return gulp.src('./app/images/**/*.+(png|jpg|jpeg|gif|svg)')
		// Caching images that ran through imagemin
		.pipe(cache(imagemin({
			interlaced: true
		})))
		.pipe(gulp.dest('./template/assets/images/'))
});





/***
 *  Fonts
 ***/
gulp.task('fonts', function () {
	return gulp.src(['./app/fonts/*'])
		.pipe(gulp.dest('./template/assets/fonts/'));
});

/**
 * Styles
 */
gulp.task('styles', function () {
	return gulp.src(['./app/css/*.css'])
		.pipe(gulp.dest('./template/assets/css/'))
});


/**
 * Script
 */
gulp.task('script', function () {
	return gulp.src(['./app/js/*.js'])
		.pipe(gulp.dest('./template/assets/js/'))
});

gulp.task('minify-css', function() {
  return gulp.src('./app/css/*.css')
    .pipe(minifyCss({compatibility: 'ie8'}))
    .pipe(gulp.dest('./template/assets/css/'));
});


gulp.task('default', function (callback) {
	runSequence(['sass', 'jade', 'browserSync', 'watch', 'images', 'fonts', 'styles', 'script', 'minify-css'],
		callback
	)
})
