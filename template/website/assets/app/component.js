app.component('mainmenu', {
        templateUrl: 'fragment/mainmenu.html',
        controller: (globalService, $scope, $translate, $timeout, language) => {
            $scope.ChangeLang = (key) => {
                $translate.use(key)
                $scope.refresh()
            }
            globalService.getLanguage()
                .then((langdata) => {
                    $scope.LangList = langdata.data.data
                })
            $scope.refresh = function() {
                $timeout(function() { window.location.reload() }, 2000)
            }
        }
    })
    .component('foot', {
        templateUrl: 'fragment/footer.html',
        controller: function() {

        }
    })
    .component('search', {
        templateUrl: 'fragment/search.html',
        controller: (globalService, $scope, getPostingList, $translate, $timeout, language) => {
            $scope.search = {
                provinceId: "",
                amplurId: "",
                postingType: "",
                placeType: "",
                bedroomFrom: "",
                bedroomTo: "",
                priceFrom: "",
                priceTo: "",
                keyworld: "",
            }
            $scope.init = () => {
                globalService.getPostingTypeList()
                    .then((postdata) => $scope.postingList = postdata.data.data)
                globalService.getPlaceTypeList()
                    .then((placedata) => $scope.placetypeList = placedata.data.data)
                globalService.getProvinceList()
                    .then((provincedata) => $scope.provinceList = provincedata.data.data)
                globalService.getBedroomList()
                    .then((bedroomdata) => $scope.bedroomList = bedroomdata.data.data)
            }
            $scope.provinceUpdate = () => globalService
                .getAmplurList($scope.search.provinceId)
                .then((amplurdata) => amplurdata.data.status == 500 ? $scope.amplurList = [] : $scope.amplurList = amplurdata.data.data)
            $scope.searchPost = () => getPostingList
                .fetchUserDetails($scope.search)
                .then((response) => console.log(response)) //หน้า ListPost
        }
    })