app = angular.module('myApp', ['pascalprecht.translate', 'ngCookies', 'ngRoute', 'ngSanitize'])
    .constant("urlAol", {
        "MainUrl": "http://52.77.78.52:8080/aol/",
        "restURL": "rest/",
        "imgUrl": "images?fileName=/"
    })
    .run(function($rootScope, urlAol) {
        $rootScope.imgUrl = urlAol.MainUrl + urlAol.imgUrl
    })
    .config(function($translateProvider) {
        $translateProvider.useStaticFilesLoader({
            prefix: 'translations/',
            suffix: '.json'
        })
        $translateProvider.preferredLanguage('TH')
        $translateProvider.useLocalStorage()
    })
    .filter('trustAsResourceUrl', ['$sce', function($sce) {
        return function(val) {
            return $sce.trustAsResourceUrl(val);
        };
    }]).config(['$routeProvider', '$locationProvider',
        function($routeProvider, $locationProvider) {
            $routeProvider.
            when('/', {
                templateUrl: 'views/index.html',
                controller: 'indexCtrl',
                controllerAs: 'vm'
            }).when('/board', {
                templateUrl: 'views/board.html',
                controller: 'boardCtrl',
                controllerAs: 'board'
            }).when('/board/:boardId', {
                templateUrl: 'views/board-detail.html',
                controller: 'boardCtrl',
                controllerAs: 'boarddetail'
            }).when('/video', {
                templateUrl: 'views/video.html',
                controller: 'videoCtrl',
                controllerAs: 'video'
            }).when('/video/:videoId', {
                templateUrl: 'views/video-detail.html',
                controller: 'videoCtrl',
                controllerAs: 'videodetail'
            }).otherwise({
                redirectTo: '/'
            })
            $locationProvider.html5Mode(true)
        }
    ])