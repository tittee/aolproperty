app.factory('language', function($translate) {
        return $translate.storage().get($translate.storageKey())
    })
    .factory("getPostingList", function($http, urlAol, language, $translate) {
        var obj = {}
        var restUrl = urlAol.MainUrl + urlAol.restURL + "postingRestService/listTransactionByPage"
        obj.fetchUserDetails = function(search) {
            return $http.get(restUrl, {
                params: {
                    local: language,
                    provinceId: search.provinceId,
                    amplurId: search.amplurId,
                    postingType: search.postingType,
                    placeType: search.placeType,
                    bedroomFrom: search.bedroomFrom,
                    bedroomTo: search.bedroomTo,
                    priceFrom: search.priceFrom,
                    priceTo: search.priceTo,
                    keyworld: search.keyword,
                    orderBy: undefined,
                    pageNo: 1,
                    rowsPerPage: 10
                }
            })
        }
        return obj
    })
    .factory("getRequirementList", function($http, urlAol, language) {
        var obj = {}
        var restURL = urlAol.MainUrl + urlAol.restURL + "public/postRequirementRestService/postRequirementList"
        obj.fetchUserDetails = () => { return $http.get(restURL, { params: { local: language, userLogin: "" } }) }
        return obj
    })
    .factory("getAcrticleNewsList", function($http, urlAol, language) { //[ 1=news,2=Article,3=Interview ]
        var obj = {}
        var restURL = urlAol.MainUrl + urlAol.restURL + "public/artilceNewsRestService/listAll"
        obj.fetchUserDetails = (articleTypeId) => { return $http.get(restURL, { params: { local: language, articleType: articleTypeId } }) }
        return obj
    })
    .service("globalService", function($http, urlAol, language) {
        this.getLanguage = () => {
            var restURL = urlAol.MainUrl + urlAol.restURL + "public/languageService/getLang"
            return $http.get(restURL, {})
        }
        this.getProvinceList = function() {
            var restURL = urlAol.MainUrl + urlAol.restURL + "public/findPropertiesDropdownRestService/provinceList"
            return $http.get(restURL, {
                params: {
                    local: language
                }
            })
        }
        this.getAmplurList = function(provinceId) {
            var restURL = urlAol.MainUrl + urlAol.restURL + "public/findPropertiesDropdownRestService/amplurList"
            return $http.get(restURL, {
                params: {
                    provinceCode: provinceId,
                    local: language
                }
            })
        }
        this.getPostingTypeList = function() {
            var restURL = urlAol.MainUrl + urlAol.restURL + "public/findPropertiesDropdownRestService/postingTypeList"
            return $http.get(restURL, { params: { local: language } })
        }
        this.getPlaceTypeList = function() {
            var restURL = urlAol.MainUrl + urlAol.restURL + "public/findPropertiesDropdownRestService/placeTypeList"
            return $http.get(restURL, {
                params: {
                    local: language
                }
            })
        }
        this.getBedroomList = function() {
            var restURL = urlAol.MainUrl + urlAol.restURL + "public/findPropertiesDropdownRestService/bedroomList"
            return $http.get(restURL, {
                params: {
                    local: language
                }
            })
        }
    })