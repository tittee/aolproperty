$(function() {
    // TABLE
    $('.star').on('click', function() {
        $(this).toggleClass('star-checked');
    });

    $('.ckbox label').on('click', function() {
        $(this).parents('tr').toggleClass('selected');
    });

    $('.btn-filter').on('click', function() {
        var $target = $(this).data('target');
        if ($target != 'all') {
            $('.table tr').css('display', 'none');
            $('.table tr[data-status="' + $target + '"]').fadeIn('slow');
        } else {
            $('.table tr').css('display', 'none').fadeIn('slow');
        }
    });

    // SLICK CAROUSEL
    $(".highlight-carousel").slick({
        dots: false,
        infinite: true,
        centerPadding: '10px',
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        centerMode: false,
        //variableWidth: true,
        responsive: [{
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
            {
                breakpoint: 762,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
    $(".video-carousel, .items-carousel").slick({
        dots: false,
        infinite: true,
        centerPadding: '10px',
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        centerMode: false,
        //variableWidth: true,
        responsive: [{
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 762,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    // SLICK DUPLICATE : PROPERTY
    $('.property-slider-top').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.property-slider-bottom'
    });
    $('.property-slider-bottom').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.property-slider-top',
        dots: true,
        centerMode: false,
        focusOnSelect: true
    });

    // // SLIPPRY SLIDER
    // var demo1 = $("#slider").slippry({
    //     // transition: 'fade',
    //     // useCSS: true,
    //     // speed: 1000,
    //     // pause: 3000,
    //     // auto: true,
    //     // preload: 'visible',
    //     // autoHover: false
    // });
});