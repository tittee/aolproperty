 app.controller('indexCtrl', function(aolService, globalService) {
     aolService.getHostListing()
         .success(function(data) {
             console.log("Host: " + data)
         })
         .error(function(data) {
             console.log("ERROR.....")
         })
     globalService.getProvinceList()
         .success(function(data) {
             console.log("Province: " + data)
         })
         .error(function(data) {
             console.log("ERROR.....")
         })
     globalService.getAmplurList(11)
         .success(function(data) {
             console.log("Amplur: " + data)
         })
         .error(function(data) {
             console.log("ERROR.....")
         })
     globalService.getPostingTypeList()
         .success(function(data) {
             console.log("PostingType: " + data)
         })
         .error(function(data) {
             console.log("ERROR.....")
         })
     globalService.getPlaceTypeList()
         .success(function(data) {
             console.log("PlaceType: " + data)
         })
         .error(function(data) {
             console.log("ERROR.....")
         })
     globalService.getBedroomList()
         .success(function(data) {
             console.log("Bedroom: " + data)
         })
         .error(function(data) {
             console.log("ERROR.....")
         })
     aolService.getPostingList({ t: 1 })
         .success(function(data) {
             console.log(data)
         })
         .error(function(data) {
             console.log("ERROR.....")
         })
     aolService.getRequirementList("admin")
         .success(function(data) {
             console.log(data)
         })
         .error(function(data) {
             console.log("ERROR.....")
         })
     aolService.getArtilceNewsList(1)
         .success(function(data) {
             console.log(data)
         })
         .error(function(data) {
             console.log("ERROR.....")
         })

 })