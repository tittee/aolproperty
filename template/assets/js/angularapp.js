app = angular.module('myApp', [])
    .value('lang', "th")
    .constant("urlAol", {
        "host": "localhost",
        "port": "8080",
        "prefix": "admin"
    })
    .service('aolService', function($http, urlAol, lang) {
        this.getHostListing = function() {
            var restURL = "http://" + urlAol.host + ":" + urlAol.port + "/" + urlAol.prefix + "/rest/public/hostListingInquiryRestService/listAll"
            return $http.get(restURL, {
                params: { branchId: 1, local: lang }
            })
        }
        this.getPostingList = function(searchObject) {
            var restURL = "http://" + urlAol.host + ":" + urlAol.port + "/" + urlAol.prefix + "/rest/postingRestService/listTransactionByPage"
            return $http.get(restURL, {
                params: {
                    local: lang,
                    provinceId: 10,
                    amplurId: null,
                    postingType: null,
                    placeType: null,
                    bedroomFrom: null,
                    bedroomTo: null,
                    priceFrom: null,
                    priceTo: null,
                    keyworld: "",
                    orderBy: undefined,
                    pageNo: 1,
                    rowsPerPage: 10
                }
            })
        }
        this.getRequirementList = function(username) {
            var restURL = "http://" + urlAol.host + ":" + urlAol.port + "/" + urlAol.prefix + "/rest/public/requirementPostRestService/requirementPostList"
            return $http.get(restURL, {
                params: { local: lang, userLogin: "admin" }
            })
        }
        this.getArtilceNewsList = (TypeId) => { //  [ 1=news,2=Article,3=Interview ]
            var restURL = "http://" + urlAol.host + ":" + urlAol.port + "/" + urlAol.prefix + "/rest/public/artilceNewsRestService/listAll"
            return $http.get(restURL, {
                params: { local: lang, articleType: 1 }
            })
        }
    })
    .service('globalService', function($http, urlAol, lang) {
        this.getProvinceList = function() {
            var restURL = "http://" + urlAol.host + ":" + urlAol.port + "/" + urlAol.prefix + "/rest/public/findPropertiesDropdownRestService/provinceList"
            return $http.get(restURL, {
                params: {
                    local: lang
                }
            })
        }
        this.getAmplurList = function(provinceId) {
            var restURL = "http://" + urlAol.host + ":" + urlAol.port + "/" + urlAol.prefix + "/rest/public/findPropertiesDropdownRestService/amplurList"
            return $http.get(restURL, {
                params: {
                    provinceCode: provinceId,
                    local: lang
                }
            })
        }
        this.getPostingTypeList = function(provinceId) {
            var restURL = "http://" + urlAol.host + ":" + urlAol.port + "/" + urlAol.prefix + "/rest/public/findPropertiesDropdownRestService/postingTypeList"
            return $http.get(restURL, {
                params: {
                    local: lang
                }
            })
        }
        this.getPlaceTypeList = function() {
            var restURL = "http://" + urlAol.host + ":" + urlAol.port + "/" + urlAol.prefix + "/rest/public/findPropertiesDropdownRestService/placeTypeList"
            return $http.get(restURL, {
                params: {
                    local: lang
                }
            })
        }
        this.getBedroomList = function() {
            var restURL = "http://" + urlAol.host + ":" + urlAol.port + "/" + urlAol.prefix + "/rest/public/findPropertiesDropdownRestService/bedroomList"
            return $http.get(restURL, {
                params: {
                    local: lang
                }
            })
        }
    })